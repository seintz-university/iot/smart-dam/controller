/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#ifndef SERVOMOTOR_H
#define SERVOMOTOR_H

class ServoMotor
{
  public:
    virtual unsigned getPosition() = 0;
    virtual void setPosition(unsigned angle) = 0;
    virtual void moveLeft(unsigned angle) = 0;
    virtual void moveRight(unsigned angle) = 0;
};

#endif
