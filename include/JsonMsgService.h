/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#ifndef JSONMSGSERVICE_H
#define JSONMSGSERVICE_H

#include "Arduino.h"
#include "ArduinoJson.h"
#include "BluetoothMsgService.h"

/**
 * A controller for json handling.
 */
class JsonMsgService {
  public:
    /**
	 * Instanciates the json controller.
	 * @param jsonMaxElems ththe max number of elements to be handled for each request
	*/
    JsonMsgService(int jsonMaxElems);

    /**
	 * Event getter.
     * @return event.
	 */
	String getEvent();

    /**
	 * Request getter.
     * @return request.
	 */
	String getRequest();

    /**
	 * Amount getter.
     * @return amount.
	 */
	unsigned getAmount();

	/**
	 * Check if a message is available.
	 * @return if a message is available.
	 */
	bool isJsonAvailable();

    /**
	 * Send an event as json.
	 * @param event the event to be sent as json.
	 */
	void sendEvent(const String& event);

    /**
	 * Send an extend as json.
	 * @param amount the amount to be sent as json.
	 */
    void sendExtend(const unsigned amount);

    /**
	 * Send the last received message back.
	 */
	void sendEcho();
    
  private:
    int capacity;
    String event;
    String request;
    unsigned amount;
    Msg* currentMsg;
};

#endif
