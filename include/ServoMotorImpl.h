/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#ifndef SERVOMOTORIMPL_H
#define SERVOMOTORIMPL_H

#define byte uint8_t
#define boolean bool

#include "ServoMotor.h"
#include "ServoTimer2.h"
#include <Arduino.h>

  class ServoMotorImpl : public ServoMotor
{
  public:
    explicit ServoMotorImpl(unsigned pin);
    unsigned getPosition() override;
    void setPosition(unsigned angle) override;
    void moveLeft(unsigned angle) override;
    void moveRight(unsigned angle) override;

  private:
    // 750 -> 0, 2250 -> 180
    // 750 + angle * (2250 - 750) / 180
    const float angleCoefficient = (2250.0 - 750.0) / 180;
    unsigned pin;
    ServoTimer2 motor;
    unsigned angle;
};

#endif

