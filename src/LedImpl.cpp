/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#include "LedImpl.h"

LedImpl::LedImpl(unsigned pin) {
    this->pin = pin;
    digitalWrite(pin, LOW);
    isOn = false;
}

void
LedImpl::switchOn() {
    digitalWrite(pin, HIGH);
    isOn = true;
}

void
LedImpl::switchOff() {
    digitalWrite(pin, LOW);
    isOn = false;
}

bool
LedImpl::isTurnedOn() {
    return isOn;
}

void LedImpl::blink(bool blink) {
    while (blink){
        this->switchOn();
        delay(1000);
        this->switchOff();
        delay(1000);
    }
}